class ConfigManager:
    _instance = None

    def __init__(self):
        self.config_file = "config.properties"
        # Load configuration settings from file or other sources

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def get_config(self, key):
        # Retrieve configuration setting based on the key
        return f"Value for {key}"


config_manager = ConfigManager.get_instance()
value1 = config_manager.get_config("setting1")
value2 = config_manager.get_config("setting2")

print(f"Setting 1: {value1}")
print(f"Setting 2: {value2}")

# Since it's a Singleton, you get the same instance no matter how many times you call get_instance()
config_manager_again = ConfigManager.get_instance()

print(config_manager is config_manager_again)  # Output: True - Both instances are the same
